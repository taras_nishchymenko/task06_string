package com.nishchymenko.strings.regex;

import java.util.Arrays;
import java.util.List;

public class RegexUtilTest {

  public static void main(String[] args) {
    RegexUtil util = new RegexUtil();
    System.out.println(util.isSentence("Aasasd."));
    System.out.println(util.isSentence("asasd."));
    System.out.println(util.isSentence("Aasasd"));
    System.out.println(util.isSentence("asasd"));

    List<String> strings = Arrays.asList("kast he lsndf kdjfn t he".split("t he"));
    System.out.println(strings);

    String replaced = "kjdvfas aefjafnaejam;g".replaceAll("[aeiouyAEIOUY]", "_");
    System.out.println(replaced);
  }

}
