package com.nishchymenko.strings.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {

  private static final String SENTENCE_REGEX = "[A-Z]+[\\w]*[.!?]";

  public boolean isSentence(String sentence) {
    Pattern pattern = Pattern.compile(SENTENCE_REGEX);
    Matcher matcher = pattern.matcher(sentence);
    return matcher.find();
  }

}
