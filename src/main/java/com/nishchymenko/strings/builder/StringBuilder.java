package com.nishchymenko.strings.builder;

import java.util.ArrayList;
import java.util.List;

public class StringBuilder {

  private List<Object> parts = new ArrayList<Object>();

  public StringBuilder plus(Object object) {
    parts.add(object);
    return this;
  }

  @Override
  public String toString() {
    String out = "";
    for (Object o : parts) {
      out += o.toString();
    }
    return out;
  }
}
