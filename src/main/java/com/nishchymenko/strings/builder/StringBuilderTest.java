package com.nishchymenko.strings.builder;

import java.time.LocalDate;

public class StringBuilderTest {

  public static void main(String[] args) {
    StringBuilder builder = new StringBuilder();
    builder.plus("asddf").plus(1).plus(1.).plus(LocalDate.now());
    System.out.println(builder.toString());
  }

}
