package com.nishchymenko.strings.i18n;

import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

public class ConsoleMenu {

  private Locale locale;

  public void launch() {
    init();
    while (true) {
      int command = readInput();
      switch (command) {
        case 0:
          continue;
        case 1:
          locale = new Locale("uk");
          break;
        case 2:
          locale = new Locale("ru");
          break;
        case 3:
          locale = new Locale("en");
          break;
        case 4:
          System.exit(0);
        default:
          System.out.println("Incorrect input!");
      }
      show();
    }
  }

  private int readInput() {
    Scanner scanner = new Scanner(System.in);
    String input = scanner.next();
    try {
      int out = Integer.parseInt(input);
      return out;
    } catch (RuntimeException e) {
      System.out.println("Not a number!");
    }
    return 0;
  }

  private void init() {
    locale = new Locale("en");
    show();
  }

  private void show() {
    ResourceBundle resourceBundle = ResourceBundle.getBundle("messages", locale);
    System.out.println(resourceBundle.getString("welcome_message"));
    System.out.println("1. " + resourceBundle.getString("change_to_uk"));
    System.out.println("2. " + resourceBundle.getString("change_to_ru"));
    System.out.println("3. " + resourceBundle.getString("change_to_en"));
    System.out.println("4. " + resourceBundle.getString("exit"));
  }

}
